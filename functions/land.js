var config = require('./config')
const requestLib = require('request');
const utils = require('./utils');

exports.processRequest = function(conv, parameters) {
    return new Promise(function(resolve, reject) {
        if (parameters.land_type !== "") {
            let land_type = parameters.land_type;
            let country = "Default";
                

            if (parameters.country !== "")
                country = parameters.country;

            console.log("Land type = " + land_type + ", region =" + country);

            var options = {
                uri: config.endpoint + "/land",
                method: 'POST',
                headers: {
                    'access-key': config.access_key
                },
                json: true,
                body: {
                    "item": land_type,
                    "region": country
                }
            };

            requestLib(options, function(error, response, body) {
                const emissionResponse = "The emissions released due to this action are given below";
                if (!error && response.statusCode === 200) {
                    console.log(body);

                    let emission = body.quantity;
                    
                    let basicResponseString = 'Emissions for ' +  land_type;
                    let finalResponseString = "";

                    if (country != "Default")
                        finalResponseString = basicResponseString + ' in ' + country + ' is ' + emission;
                    else
                        finalResponseString = basicResponseString + ' is ' + emission;


                    let outputUnit = body.unit;
                    if (outputUnit !== undefined) {
                        finalResponseString = finalResponseString + ' ' + outputUnit;
                        utils.richResponse(conv, finalResponseString, emissionResponse);
                        resolve();
                    } else {
                        finalResponseString = finalResponseString + ' kg'
                        utils.richResponse(conv, finalResponseString, emissionResponse);
                        resolve();
                    }
                } else {
                  // Handle errors here
                  utils.handleError(error, response, body, conv);
                  resolve();
                }
            });

        } else {
            conv.ask("Sorry, I did not understand the Land type you said.");
            resolve();
        }
    });
}
